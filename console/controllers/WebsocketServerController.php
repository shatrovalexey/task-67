<?php
namespace console\controllers ;

use yii ;
use yii\helpers\Console ;
use yii\console\Controller ;
use yii\filters\VerbFilter ;
use console\controllers\CommandsWebsocketServerController ;

use consik\yii2websocket\events\WSClientMessageEvent ;
use consik\yii2websocket\WebSocketServer ;

class WebsocketServerController extends Controller {
	public function actionStart( $port = 9998 ) {
		$server = new CommandsWebsocketServerController( ) ;
		$server->port = $port ;

		$server->on( WebSocketServer::EVENT_WEBSOCKET_OPEN_ERROR , function( $evt ) use( $server ) {
			echo "Error opening port " . $server->port . "\n" ;
			$server->port ++ ;
			$server->start( ) ;
		} ) ;

		$server->on( WebSocketServer::EVENT_WEBSOCKET_OPEN , function( $evt ) use( $server ) {
			echo "Server started at port " . $server->port ;
		} ) ;

		$server->start( ) ;
	}
}
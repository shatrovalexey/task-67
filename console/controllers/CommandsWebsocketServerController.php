<?php
namespace console\controllers ;

use yii ;
use yii\helpers\Console ;
use yii\console\Controller ;
use yii\filters\VerbFilter ;

use consik\yii2websocket\WebSocketServer ;
use Ratchet\ConnectionInterface ;
use common\models\Apple ;

class CommandsWebsocketServerController extends WebSocketServer {
	/**
	* форматирование ответа клиенту и отправка
	* @param ConnectionInterface $client - объект подключения клиента
	* @param mixed $msg - сообщение клиенту
	*/
	protected function send( ConnectionInterface $client , $msg , $action ) {
		$msg = json_encode( [
			'action' => $action ,
			'result' => $msg ,
		] ) ;

		$client->send( $msg ) ;
	}

	/**
	* раскодирование запроса клиента
	* @param string $msg - сообщение от клиента
	*/
	protected function msg( $msg ) {
		return @json_decode( $msg ) ;
	}

	/**
	* команда от клиента
	*
	* @param ConnectionInterface $client - объект подключения клиента
	* @param string $msg - сообщение от клиента
	*
	* @return string - название команды
	*/
	protected function getCommand( ConnectionInterface $client , $msg ) {
		$request = $this->msg( $msg ) ;

		if ( empty( $request->action ) ) {
			return parent::getCommand( $client , $msg ) ;
		}

		echo $request->action . PHP_EOL ;

		return $request->action ;
	}

	/**
	* Список яблок
	*
	* @param ConnectionInterface $client - объект подключения клиента
	* @param mixed $msg - сообщение от клиента
	*/
	public function commandList( ConnectionInterface $client , $msg ) {
		$result = Apple::find( )->asArray( )->all( ) ;
		$msg = $this->msg( $msg ) ;

		try {
			$msg = json_encode( [
				'action' => $msg->action ,
				'result' => $result ,
			] ) ;

			$client->send( $msg ) ;
		} catch ( \Exception $exception ) {
			echo $exception->getMessage( ) . PHP_EOL ;
		}

		// $this->send( $client , $result , $msg->action ) ;
	}

	/**
	* Соравать яблоко
	*
	* @param ConnectionInterface $client - объект подключения клиента
	* @param mixed $msg - сообщение от клиента
	*/
	public function commandDrop( ConnectionInterface $client , $msg ) {
		$msg = $this->msg( $msg ) ;
		$result = [
			'result' => false ,
		] ;

		try {
			$apple = Apple::findOne( $msg->id ) ;

			$dbh = \Yii::$app->db->masterPdo ;
			$sth = $dbh->prepare( "
SELECT
	`a1`.*
FROM
	`apple` AS `a1`
WHERE
	( `a1`.`id` = :id )
LIMIT 1 ;
			" ) ;

			$sth->execute( [
				':id' => $msg->id ,
			] ) ;

			while ( $row = $sth->fetch( \PDO::FETCH_OBJ ) ) {
				$apple->id = $row->id ;
				$apple->color = $row->color ;
				$apple->size = $row->size ;
				$apple->state = $row->state ;
				$apple->created_at = $row->created_at ;
				$apple->updated_at = $row->updated_at ;
			}
			$sth->closeCursor( ) ;

			$result[ 'result' ] = $apple->drop( ) ;
		} catch ( \Exception $exception ) {
			echo $exception->getMessage( ) . PHP_EOL ;

			$result[ 'error' ] = $exception->getMessage( ) ;
		}

		$this->send( $client , $result , $msg->action ) ;
	}

	/**
	* Съесть яблоко
	*
	* @param ConnectionInterface $client - объект подключения клиента
	* @param mixed $msg - сообщение от клиента
	*/
	public function commandEat( ConnectionInterface $client , $msg ) {
		$msg = $this->msg( $msg ) ;

		$apple = Apple::find( )->where( [
			'id' => $msg->id ,
		] )->one( ) ;

		$dbh = \Yii::$app->db->masterPdo ;
		$sth = $dbh->prepare( "
SELECT
	`a1`.*
FROM
	`apple` AS `a1`
WHERE
	( `a1`.`id` = :id )
LIMIT 1 ;
		" ) ;

		$sth->execute( [
			':id' => $msg->id ,
		] ) ;

		while ( $row = $sth->fetch( \PDO::FETCH_OBJ ) ) {
			$apple->id = $row->id ;
			$apple->color = $row->color ;
			$apple->size = $row->size ;
			$apple->state = $row->state ;
			$apple->created_at = $row->created_at ;
			$apple->updated_at = $row->updated_at ;
		}
		$sth->closeCursor( ) ;

		$result = [
			'result' => false ,
		] ;

		try {
			$result[ 'result' ] = $apple->eat( $msg->size ) ;
		} catch ( \Exception $exception ) {
			$result[ 'error' ] = $exception->getMessage( ) ;
		}

		$this->send( $client , $result , $msg->action ) ;
	}

	/**
	* Посеять яблоки
	*
	* @param ConnectionInterface $client - объект подключения клиента
	* @param mixed $msg - сообщение от клиента
	*/
	public function commandSeed( ConnectionInterface $client , $msg ) {
		$msg = $this->msg( $msg ) ;

		$result = Apple::seed( $msg->count ) ;

		$this->send( $client , $result , $msg->action ) ;
	}
}

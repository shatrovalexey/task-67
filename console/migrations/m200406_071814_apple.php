<?php

use yii\db\Migration;

/**
 * Class m200406_071814_apple
 */
class m200406_071814_apple extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp( ) {
		\Yii::$app->db->masterPdo->exec( "
CREATE TABLE `apple`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`color` CHAR( 7 ) NOT null COMMENT 'цвет' ,
	`state` TINYINT( 1 ) UNSIGNED NOT null COMMENT 'статус' ,
	`size` MEDIUMINT UNSIGNED NOT null COMMENT 'размер' ,
	`created_at` INTEGER NOT null COMMENT 'дата-время создания' ,
	`updated_at` INTEGER NOT null COMMENT 'дата-время изменения' ,

	PRIMARY KEY( `id` ) ,
	INDEX( `state` , `updated_at` )
) COMMENT 'яблоко' ;
		" ) ;
	}

    /**
     * {@inheritdoc}
     */
	public function safeDown( ) {
		\Yii::$app->db->masterPdo->exec( "
DROP TABLE IF EXISTS `apple` ;
		" ) ;
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200406_071814_apple cannot be reverted.\n";

        return false;
    }
    */
}

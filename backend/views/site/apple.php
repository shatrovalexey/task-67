<?php

/* @var $this yii\web\View */

use common\models\Apple ;

$this->title = 'Яблоки на снегу';
?>
<style>
.list {
	list-style-type: none ;
}
.list li {
	list-style-type: none ;
	text-align: center ;
	padding: 7% ;
	border-radius: 100% ;
	border: 1px #000000 solid ;
	margin: 10px ;
	display: block ;
	font-color: #000000 !important ;
	font-weight: bold ;
}
.list li:nth-of-type( odd ) {
	float: left ;
}
.list li:nth-of-type( even ) {
	float: right ;
}
.list .list-item-size ,
.list .list-item-drop ,
.list .list-item-eat {
	background-color: #ffffff ;
	display: block ;
	margin: 1px ;
	border-radius: 5% ;
}
.list .list-item-drop ,
.list .list-item-eat {
	cursor: pointer ;
}
</style>
<div class="layout">
	<form id="form-seed">
		<fieldset>
			<label>
				<span>количество:</span>
				<input name="count" type="number" value="10" min="1" max="200" required>
			</label>
			<label>
				<span>посеять</span>
				<input type="submit" value="&rarr;">
			</label>
		</fieldset>
	</form>
	<ul class="list"></ul>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
jQuery( function( ) {
	let $wsh ;
	let $actions ;
	let $list = jQuery( ".list" ) ;

	$actions = {
		"make_drop" : function( ) {
			let $self = jQuery( this ) ;
			let $item = $self.parents( ":first" ) ;

			$wsh.send( JSON.stringify( {
				"action" : "drop" ,
				"id" : $item.data( "id" )
			} ) ) ;
		} ,
		"make_eat" : function( ) {
			let $self = jQuery( this ) ;
			let $item = $self.parents( ":first" ) ;
			let $size = $self.data( "size" ) ;
			let $prompt_size = prompt( "Укажите, сколько съесть" ) ;

			if ( ( $prompt_size <= 0 ) || ( $prompt_size > $size ) ) {
				alert( "Указан неверный размер" ) ;

				return ;
			}

			$size = $prompt_size ;

			$wsh.send( JSON.stringify( {
				"action" : "eat" ,
				"id" : $item.data( "id" ) ,
				"size" : $size ,
			} ) ) ;
		} ,
		"list" : function( $data ) {
			$list.empty( ) ;

			for ( let $result_item of $data.result ) {
				let $item = jQuery( "<li>" ).attr( {
					"data-color" : $result_item.color ,
					"data-state" : $result_item.state ,
					"data-size" : $result_item.size ,
					"data-id" : $result_item.id
				} ).css( {
					"background-color" : $result_item.color ,
					"border-width" : String( $result_item.state ).concat( "px" ) ,
				} ) ;

				let $size = jQuery( "<div>" ).addClass( "list-item-size" )
					.text( String( "размер " ).concat( $result_item.size ) ) ;

				$item.append( $size ) ;

				switch ( $result_item.state ) {
					case "<?=Apple::STATE_NEW?>" : {
						let $drop = jQuery( "<div>" ).addClass( "list-item-drop" )
							.text( "упасть" ).on( "click" , $actions.make_drop ) ;
						$item.append( $drop ) ;

						break ;
					}
					case "<?=Apple::STATE_ATE?>" :
					case "<?=Apple::STATE_DROPPED?>" : {
						let $eat = jQuery( "<div>" ).addClass( "list-item-eat" )
							.text( "съесть" ).on( "click" , $actions.make_eat ) ;
						$item.append( $eat ) ;

						let $left = jQuery( "<div>" ).addClass( "list-item-updated_at" ).text(
							String( "гниёт " )
								.concat( 5 * 60 * 60 - ( Date.parse( new Date( ) ) / 1e3 - $result_item.updated_at ) )
						) ;
						$item.append( $left ) ;

						break ;
					}
					default : {
						alert( $result_item.state ) ;

						break ;
					}
				}

				let $state = jQuery( "<div>" ).addClass( "list-item-state" ).text( {
					"<?=Apple::STATE_NEW?>" : "висит" ,
					"<?=Apple::STATE_DROPPED?>" : "упало" ,
					"<?=Apple::STATE_ATE?>" : "накушено"
				}[ $result_item.state ] ) ;
				$item.append( $state ) ;

				$list.append( $item ) ;
			}
		} ,
		"list_get" : function( $continue ) {
			$wsh.send( JSON.stringify( {
				"action" : "list"
			} ) ) ;

			if ( $continue ) {
				setTimeout( function( ) {
					$actions.list_get( $continue ) ;
				} , 4e3 ) ;
			}
		} ,
		"drop" : function( ) {
			$actions.list_get( ) ;
		} ,
		"eat" : function( ) {
			$actions.list_get( ) ;
		} ,
		"connect" : function( ) {
			console.log( "Connecting" ) ;

			$wsh = new WebSocket( "ws://<?=\Yii::$app->request->getHostName( )?>:9998" ) ;
			$wsh.onopen = function( ) {
				console.log( "Connected" ) ;

				$actions.list_get( true ) ;
			} ;
			$wsh.onmessage = function( $evt ) {
				let $data = JSON.parse( $evt.data ) ;

				try {
					$actions[ $data.action ]( $data ) ;
				} catch ( $exception ) {
					console.log( $exception ) ;
				}
			} ;
			$wsh.onerror = function( $evt ) {
				setTimeout( $actions.connect , 0.5e3 ) ;
			} ;
		} 
	} ;

	$actions.connect( ) ;

	jQuery( "#form-seed" ).on( "submit" , function( ) {
		let $self = jQuery( this ) ;
		let $count = $self.find( "[name=count]" ).val( ) ;
		let $msg = JSON.stringify( {
			"action" : "seed" ,
			"count" : $count
		} ) ;

		$wsh.send( $msg ) ;

		return false ;
	} ) ;
} ) ;
</script>
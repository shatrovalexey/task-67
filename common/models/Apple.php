<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
* Яблоко
*
* @package Apple
* @author Shatrov Aleksej <mail@ashatrov.ru>
*/

class Apple extends ActiveRecord {
	/**
	* @const integer STATE_NEW - новое
	*/
	const STATE_NEW = 0 ;

	/**
	* @const integer STATE_ATE - съедено
	*/
	const STATE_ATE = 1 ;

	/**
	* @const integer STATE_DROPPED - упало
	*/
	const STATE_DROPPED = 2 ;

	/**
	* @const integer STATE_DUSTED - сгнило
	*/
	const STATE_DUSTED = 3 ;

	/**
	* @const integer TIME_TO_DUST - срок гниения в секундах
	*/
	const TIME_TO_DUST = 5 * 60 * 60 ;

	/**
	* @const integer MIN_SIZE - минимальный размер
	*/
	const MIN_SIZE = 1 ;

	/**
	* @const integer MAX_SIZE - максимальный размер
	*/
	const MAX_SIZE = 100 ;

	/**
	* @param array of string $colors - допустимые цвета
	*/
	protected $colors = [ '#FF0000' , '#00FF00' , '#0000FF' , ] ;

	/**
	* @param integer - идентификатор
	*/
	public $id ;

	/**
	* @param string - цвет
	*/
	public $color ;

	/**
	* @param integer - размер
	*/
	public $size ;

	/**
	* @param integer - статус
	*/
	public $state ;

	/**
	* {@inheritdoc}
	*/
	public static function tableName( ) {
		return 'apple' ;
	}

	public static function primaryKey( ) {
		return [ 'id' , ] ;
	}

	/**
	* {@inheritdoc}
	*/
	public function behaviors( ) {
		return [
			TimestampBehavior::className( ) ,
		] ;
	}

	/**
	* {@inheritdoc}
	*/
	public function rules( ) {
		return [
			[ 'size' , 'default' , 'value' => rand( self::MIN_SIZE , self::MAX_SIZE ) , ] ,
			[ 'state' , 'default' , 'value' => self::STATE_NEW , ] ,
			[ 'color' , 'default' , 'value' => $this->colors[ array_rand( $this->colors ) ] , ] ,
		] ;
	}

	/**
	* {@inheritdoc}
	*/
	public function init( ) {
		parent::init( ) ;

		try {
			$this->dust( ) ;
		} catch ( \Exception $exception ) {
		}
	}

	/**
	* изменить статус
	*
	* @throws \Exception
	* @return Apple
	*/
	protected function setState( $state ) {
		switch ( $this->state ) {
			case self::STATE_ATE :
			case self::STATE_DROPPED : {
				if ( $state == self::STATE_ATE ) {
					break ;
				}

				throw new \Exception( 'Нельзя съесть не упавшее' ) ;
			}
			case self::STATE_NEW : {
				if ( $state == self::STATE_DROPPED ) {
					break ;
				}

				throw new \Exception( 'Не может упасть уже упавшее' ) ;
			}
			case self::STATE_DUSTED : {
				if ( $state == self::STATE_DROPPED ) {
					break ;
				}

				if ( ( time( ) - $this->updated_at ) >= self::TIME_TO_DUST ) {
					break ;
				}

				throw new \Exception( 'Неупавшее не может сгнить или сгнить не вовремя' ) ;
			}
			default : {
				throw new \Exception( 'Неизвестный статус' ) ;
			}
		}

		$this->state = $state ;
		$this->update( ) ;

		\Yii::$app->db->masterPdo->exec( "
UPDATE
	`apple` AS `a1`
SET
	`a1`.`state` := " . intval( $state ) . " ,
	`a1`.`updated_at` := CASE " . intval( $state ) . "
		WHEN 2 THEN
			unix_timestamp( )
		ELSE
			`a1`.`updated_at`
	END
WHERE
	( `a1`.`id` = " . intval( $this->id ) . " ) ;
		" ) ;

		return $this ;
	}

	/**
	* сгнить
	*
	* @return Apple
	*/
	protected function dust( ) {
		return $this->setState( self::STATE_DUSTED ) ;
	}

	/**
	* упасть
	*
	* @return Apple
	*/
	public function drop( ) {
		return $this->setState( self::STATE_DROPPED ) ;
	}

	/**
	* съесть
	*
	* @param integer $size - сколько съесть
	*
	* @return Apple
	*/
	public function eat( $size ) {
		$this->setState( self::STATE_ATE ) ;

		if ( $this->size < $size ) {
			throw new \Exception( 'Нельзя съесть больше ем есть' ) ;
		}

		$this->size -= $size ;

		if ( empty( $this->size ) ) {
			$this->delete( ) ;
		} else {
			\Yii::$app->db->masterPdo->exec( "
UPDATE
	`apple` AS `a1`
SET
	`a1`.`size` := " . intval( $this->size ) . "
WHERE
	( `a1`.`id` = " . intval( $this->id ) . " ) ;
			" ) ;
		}

		return $this ;
	}

	/**
	* посеять яблоки
	*
	* @param integer $count - сколько посеять
	*
	* @return boolean
	*/
	public static function seed( $count ) {
		self::deleteAll( ) ;
		$result = 0 ;

		while ( $count -- > 0 ) {
			$apple = new self( ) ;
			$apple->save( ) ;
			$result ++ ;
		}

		return $result ;
	}
}
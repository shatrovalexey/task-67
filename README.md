### ЗАДАЧА
https://docs.google.com/document/d/1XgSKkfZ7aXSBOB3WrOF9PrK1irFd2FU3w7KqfyPm6PQ/edit

### УСТАНОВКА И ЗАПУСК
```
git clone https://bitbucket.org/shatrovalexey/task-67.git
composer install
# настроить подключение к СУБД
./init #advanced
./yii migrate
nohup ./yii websocket-server/start &
# запустить веб-сервер для папки backend/web
# авторизоваться на странице /admin/login
```

### АВТОР
Шатров Алексей Сергеевич <mail@ashatrov.ru>